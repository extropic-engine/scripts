#!/usr/bin/env ruby

require 'mail'
require './mailer_config.rb' # put your mail settings in here

# TODO: figure out how to store todo list

Mail.all.each do |email|
  if $trusted_senders.include? email.sender
    if email.subject == 'todo add'
      # TODO: add body to todo list
    elsif email.subject == 'todo get'
      # TODO: reply with current contents of todo list
    elsif email.subject == 'todo finish'
      # TODO: get todo reference from body and mark it as finished
    else
      Mail.deliver do
        from    $me
        to      email.sender
        subject 'Operation not supported.'
        body    'The operation ' + mail.subject + ' is not supported.'
        # TODO: add list of supported operations
      end
    end
  end
end
