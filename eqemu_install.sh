#!/bin/sh

# TODO: check dependencies

# TODO: configure directories
echo Enter the directory to install to, or leave blank for default. \(~/eqemu\)
read INSTALL_DIR
if [ "$INSTALL_DIR" == "" ]; then
INSTALL_DIR=~/eqemu
fi

if [ ! -d "$INSTALL_DIR" ]; then
  while true; do
    read -p "Directory $INSTALL_DIR does not exist, create it? " yn
    case $yn in
      [Yy]* ) mkdir $INSTALL_DIR; break;;
      [Nn]* ) echo Installation failed.; exit;;
      * ) echo Please enter Y or N.;;
    esac
  done
fi

# TODO: download eqemu sources
cd $INSTALL_DIR
svn checkout http://projecteqemu.googlecode.com/svn/trunk/ projecteqemu
# TODO: build
