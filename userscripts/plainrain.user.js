// ==UserScript==
// @name           PlainRain
// @version        1.0.2
// @description    Hides extra content on rainymood.com
// @author         Nova <nova@extropicstudios.com>
// @include        http://www.rainymood.com/
// @grant          none
// @downloadURL    https://gitlab.com/extropic-engine/scripts/raw/master/userscripts/plainrain.user.js
// ==/UserScript==
document.title = "Rainy Mood";
document.getElementById('myContent').style.visibility = 'hidden';
document.getElementById('footer').style.visibility = 'hidden';
