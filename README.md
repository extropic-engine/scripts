todo.rb
=======

Searches a project directory for TODO comments and prints them nicely formatted and organized by file.

Usage:

    todo.rb <flags> <project directory>

Flags:

    -c : compact mode