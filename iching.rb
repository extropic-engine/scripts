#!/usr/bin/env ruby

def println line
  line.each do |char|
    print char
    sleep 0.1
  end
end

def flip
    2 + (rand 2)
end

def line
    flip + flip + flip
end

def trigram_array_to_index(trigram)
    trigram = trigram.map do |x| x % 2 end
    trigram = trigram.to_s.delete('[], ')
    trigram_table = {
        '000' => :earth,
        '001' => :thunder,
        '010' => :water,
        '011' => :lake,
        '100' => :mountain,
        '101' => :fire,
        '110' => :wind,
        '111' => :heaven
    }
    trigram_table[trigram]
end

def reverse_changed(gram)
    rev = gram.map do |x|
        if x == 9 then
            6
        elsif x == 6 then
            9
        else
            x
        end
    end
    rev
end

def generate_hexagram
    [line, line, line, line, line, line]
end

# TODO: add parameter show_changed
def print_lines(hexagram, show_changed=true)
    hexagram.each do |x|
      if x % 2 == 0 then
        print '--  --'.center(80).rstrip
      else
        print '------'.center(80).rstrip
      end
      print ' *' if (x == 6 or x == 9) and show_changed
      print "\n"
    end
end

def print_description(hexagram)
    upper = trigram_array_to_index [hexagram[3], hexagram[4], hexagram[5]]
    lower = trigram_array_to_index [hexagram[0], hexagram[1], hexagram[2]]
    puts (upper.to_s.upcase + ' / ' + lower.to_s.upcase).center(80) + "\n\n"
    hexagram_data = {
      [:earth,    :earth]    => [2,  "K'UN", "THE RECEPTIVE", "BEAR WITH THINGS AS THE EARTH BEARS WITH US:\nBY YIELDING, BY ACCEPTING, BY NOURISHING"],
      [:earth,    :thunder]  => [24, "FU", "RETURN", "A TIME OF DARKNESS COMES TO A CLOSE"],
      [:earth,    :water]    => [7,  "SHIH", "THE ARMY", "IN TIMES OF WAR IT IS DESIRABLE\nTO BE LED BY A CAUTIOUS AND HUMANE GENERAL"],
      [:earth,    :lake]     => [19, "LIN", "APPROACH", "GOOD APPROACHES THE SUPERIOR PERSON"],
      [:earth,    :mountain] => [15, "CH'IEN", "MODESTY", "THE CREATIVE ACTS TO EMPTY WHAT IS FULL\nAND TO OFFER ABUNDANCE TO WHAT IS MODEST"],
      [:earth,    :fire]     => [36, "", "", ""],
      [:earth,    :wind]     => [46, "", "", ""],
      [:earth,    :heaven]   => [11, "T'AI", "PEACE", "HEAVEN EXISTS ON EARTH FOR THOSE WHO\nMAINTAIN CORRECT THOUGHTS AND ACTIONS"],
      [:thunder,  :earth]    => [16, "YU", "ENTHUSIASM", "PROPER ENTHUSIASM OPENS EVERY DOOR"],
      [:thunder,  :thunder]  => [51, "", "", ""],
      [:thunder,  :water]    => [40, "", "", ""],
      [:thunder,  :lake]     => [54, "", "", ""],
      [:thunder,  :mountain] => [62, "", "", ""],
      [:thunder,  :fire]     => [55, "", "", ""],
      [:thunder,  :wind]     => [32, "HENG", "DURATION", "REMAIN STEADY AND ALLOW\nTHE WORLD TO SHAPE ITSELF"],
      [:thunder,  :heaven]   => [34, "TA CHUANG", "THE POWER OF THE GREAT", "TO ACHIEVE TRUE POWER AND TRUE GREATNESS\nONE MUST BE IN HARMONY WITH WHAT IS RIGHT"],
      [:water,    :earth]    => [8,  "PI", "HOLDING TOGETHER (UNION)", "SEEK UNION WITH OTHERS AND THE SAGE"],
      [:water,    :thunder]  => [3,  "CHUN", "DIFFICULTY AT THE BEGINNING", "IF WE PERSEVERE A GREAT SUCCESS IS AT HAND"],
      [:water,    :water]    => [29, "K'AN", "THE ABYSMAL (WATER)", "FLOW LIKE PURE WATER\nTHROUGH DIFFICULT SITUATIONS"],
      [:water,    :lake]     => [60, "", "", ""],
      [:water,    :mountain] => [39, "", "", ""],
      [:water,    :fire]     => [63, "", "", ""],
      [:water,    :wind]     => [48, "", "", ""],
      [:water,    :heaven]   => [5,  "HSU", "WAITING (NOURISHMENT)", "TO WAIT WITH A PROPER ATTITUDE\nINVITES THE ASSISTANCE OF THE HIGHER POWER"],
      [:lake,     :earth]    => [45, "", "", ""],
      [:lake,     :thunder]  => [17, "SUI", "FOLLOWING", "DO NOT ARGUE WITH WHAT IS;\nSIMPLY FOLLOW THE PROGRESS OF TRUTH"],
      [:lake,     :water]    => [47, "", "", ""],
      [:lake,     :lake]     => [58, "", "", ""],
      [:lake,     :mountain] => [31, "HSIEN", "INFLUENCE (WOOING)", "AN INFLUENCE COMES.\nGOOD FORTUNE TO THOSE\nWHOSE HEARTS ARE CORRECT"],
      [:lake,     :fire]     => [49, "", "", ""],
      [:lake,     :wind]     => [28, "TA KUO", "PREPONDERANCE OF THE GREAT", "THERE ARE GREAT PRESSURES AT WORK.\nBY MEETING THEM WITH MODESTY AND PATIENCE,\nYOU AVOID MISFORTUNE AND MEET WITH SUCCESS"],
      [:lake,     :heaven]   => [43, "", "", ""],
      [:mountain, :earth]    => [23, "PO", "SPLITTING APART", "DO NOT ATTEMPT TO INTERVENE NOW"],
      [:mountain, :thunder]  => [27, "I", "THE CORNERS OF THE MOUTH (PROVIDING NOURISHMENT)", "GIVE PROPER NOURISHMENT TO YOURSELF AND OTHERS"],
      [:mountain, :water]    => [4,  "MENG", "YOUTHFUL FOLLY", "EVEN THE FOOLISH CAN ATTAIN WISDOM\nBY MODESTLY FOLLOWING THE SAGE"],
      [:mountain, :lake]     => [41, "", "", ""],
      [:mountain, :mountain] => [52, "", "", ""],
      [:mountain, :fire]     => [22, "PI", "GRACE", "INSIDE, THE STRENGTH OF SIMPLICITY AND SELF-KNOWLEDGE.\nOUTSIDE, THE BEAUTY OF ACCEPTANCE AND GENTLENESS"],
      [:mountain, :wind]     => [18, "KU", "WORK ON WHAT HAS BEEN SPOILED (DECAY)", "A CHALLENGE TO IMPROVEMENT:\nTHAT WHICH HAS BEEN SPOILED THROUGH NEGLECT\nCAN BE REJUVENATED THROUGH EFFORT"],
      [:mountain, :heaven]   => [26, "TA CH'U", "THE TAMING POWER OF THE GREAT", "IN THE FACE OF RISING TENSION, KEEP STILL.\nHONOR IN PRACTICE WHAT YOU HAVE LEARNED FROM THE I CHING"],
      [:fire,     :earth]    => [35, "CHIN", "PROGRESS", "YOU PROGRESS LIKE THE RISING SUN.\nTHE BRIGHTER YOUR VIRTUE, THE HIGHER YOU RISE"],
      [:fire,     :thunder]  => [21, "SHIH HO", "BITING THROUGH", "THERE IS NO OBSTACLE TO THE EXPRESSION OF TRUTH.\nWITHDRAWAL INTO QUIETNESS ALLOWS THE SAGE TO MODERATE"],
      [:fire,     :water]    => [64, "", "", ""],
      [:fire,     :lake]     => [38, "", "", ""],
      [:fire,     :mountain] => [56, "", "", ""],
      [:fire,     :fire]     => [30, "LI", "THE CLINGING, FIRE", "CLING TO THE POWER OF HIGHER TRUTH"],
      [:fire,     :wind]     => [50, "", "", ""],
      [:fire,     :heaven]   => [14, "TA YU", "POSSESSION IN GREAT MEASURE", "THOSE WHO ARE STEADFASTLLY BALANCED, HUMBLE,\nAND IN HARMONY WITH THE SAGE INHERIT\nEVERYTHING UNDER THE SUN"],
      [:wind,     :earth]    => [20, "KUAN", "CONTEMPLATION", "BY CONCENTRATING ON THE HIGHER LAWS\nYOU ACQUIRE THE POWER THAT UNDERLIES THEM"],
      [:wind,     :thunder]  => [42, "", "", ""],
      [:wind,     :water]    => [59, "", "", ""],
      [:wind,     :lake]     => [61, "", "", ""],
      [:wind,     :mountain] => [53, "", "", ""],
      [:wind,     :fire]     => [37, "", "", ""],
      [:wind,     :wind]     => [57, "", "", ""],
      [:wind,     :heaven]   => [9,  "HSIAO CH'U", "THE TAMING POWER OF THE SMALL", "YOU ARE TEMPORARILY RESTRAINED.\nIT IS A TIME FOR TAKING SMALL STEPS"],
      [:heaven,   :earth]    => [12, "P'I", "STANDSTILL (STAGNATION)", "IN TIMES OF STAGNATION, ATTEND TO YOUR ATTITUDE"],
      [:heaven,   :thunder]  => [25, "WU WANG", "INNOCENCE (THE UNEXPECTED)", "ALL GOOD COMES WHEN WE ARE INNOCENT"],
      [:heaven,   :water]    => [6,  "SUNG", "CONFLICT", "THE PROPER RESPONSE TO CONFLICT, WHETHER\nIT LIES WITHIN OR WITHOUT US, IS DISENGAGEMENT"],
      [:heaven,   :lake]     => [10, "LU", "TREADING (CONDUCT)", "LASTING PROGRESS IS WON\nTHROUGH QUIET SELF-DISCIPLINE"],
      [:heaven,   :mountain] => [33, "TUN", "RETREAT", "THIS IS A TIME FOR DISENGAGEMENT AND RETREAT.\nIN STILLNESS YOU ARE OUT OF REACH OF DANGER"],
      [:heaven,   :fire]     => [13, "T'UNG JEN", "FELLOWSHIP WITH OTHERS", "IN FELLOWSHIP WITH OTHERS,\nEMBODY THE PRINCIPLES OF THE SAGE"],
      [:heaven,   :wind]     => [44, "", "", ""],
      [:heaven,   :heaven]   => [1,  "CH'IEN", "THE CREATIVE", "IF YOU ARE ALERT TO THE CREATIVE\nYOU WILL MEET WITH GOOD FORTUNE NOW"],
    }
    puts (hexagram_data[[upper, lower]][0].to_s + ". " + hexagram_data[[upper, lower]][1]).center(80) + "\n"
    puts hexagram_data[[upper, lower]][2].center(80) + "\n"
    hexagram_data[[upper, lower]][3].split("\n").each do |x|
      puts x.center(80) + "\n"
    end
    puts "\n"
end

def print_primary(hexagram, show_changed=true)
    print_lines hexagram, show_changed
    print_description hexagram
end

def print_changed(hexagram)
end

def print_hexagram(hexagram)
    rev = reverse_changed hexagram

    print_primary hexagram
    print_changed hexagram
    print_primary rev, false if rev != hexagram
end

quit = false
while !quit do
  puts "\n===============================================================================\n\n"
    puts 'ELECTRONIC I CHING'.center(80)
    puts 'SELECT MENU OPTION'.center(80)
    puts '------------------'.center(80)
    puts '1. GENERATE HEXAGRAM'.center(80)
    puts '2. INPUT HEXAGRAM'.center(80)
    puts '3. QUIT'.center(80)
    choice = gets.to_i
    if choice == 1 then
        hexagram = generate_hexagram
        print_hexagram hexagram
    elsif choice == 2 then
        puts 'INPUT HEXAGRAM NUMBER'.center(80)
        gets hexagram
        print_hexagram hexagram
    elsif choice == 3 then
        quit = true
    end
end
