#!/usr/bin/env ruby

require 'rubygems'
require 'colorize'
require 'bucketface'
#require 'todo_config.rb'

# Scans all .cpp and .h files in the src directory to find lines marked TODO
# Future extensions:
# Create a report file that logs the number of TODOs, the date, and the total number of lines/files.
# Have the report run on every git commit (tagging the report with the commit number) or with cron
# Match TODOs to bitbucket or git issues

##########################
# Configuration settings #
##########################

$filenames = ["Cakefile", "Makefile"]
$filetypes = [".c", ".coffee", ".cpp", ".css", ".less", ".h", ".hpp", ".hs", ".htm", ".html", ".java", ".js", ".pde", ".php", ".py", ".rb", ".sh", ".m", ".mm"]
$directories = ["."]

$total_todo_count = 0
$total_line_count = 0

########################
# Function definitions #
########################

def list_todos(compact=false)
  Dir.entries(".").each do |dir|
    next if dir == "." or dir == ".."

    if File.directory?(dir) then
      Dir.chdir(dir)
      list_todos(compact)
      Dir.chdir("..")
    else
      if $filetypes.include? File.extname(dir) or $filenames.include? File.basename(dir)
        File.open(dir) do |file|
          dir_shown = false
          line_number = 1
          file_todo_count = 0
          while line = file.gets do
            if line.include? "TODO" then
              line.strip!
              line.sub!(/^\/\//, '')
              line.sub!(/^\#/, '')
              line.strip!
              if dir_shown == false then
                # TODO: don't print whole directory, just from current directory
                puts (Dir.pwd + "/" + dir).magenta if compact == false
                dir_shown = true
              end
              file_todo_count += 1
              $total_todo_count += 1
              if compact == true then
                # TODO: format this in columns
                puts dir.to_s.magenta + " " + line_number.to_s.green + " " + line
              else
                puts "  " + line_number.to_s.green + "\t" + line
              end
            end
            line_number += 1
            $total_line_count += 1
          end
          # TODO: figure out how to round to two significant figures
          if file_todo_count > 0 and compact == false then
            puts file_todo_count.to_s + " TODOs (" + (file_todo_count.to_f / line_number.to_f).round(2).to_s + "%) in " + dir.to_s
            puts
          end
        end
      end
    end
  end
end

def show_usage
  puts "    Usage: todo.rb <flags> <projet base directory>"
  puts "    Flags:"
  puts "        -c : compact mode"
end

##################
# Run the script #
##################

# TODO: command line arguments should filter which directories/files to check

# Log into bitbucket
#client = Bucketface::Client.new(:login => $bitbucket_login :password => $bitbucket_password)

compact = false
if ARGV.size == 1
  Dir.chdir ARGV[0]
elsif ARGV.size == 2
  if ARGV[0] == "-c"
    compact = true
    Dir.chdir ARGV[1]
  else
    show_usage
    exit
  end
else
  show_usage
  exit
end

$directories.each do |dir|
  if not File.directory? dir
    puts dir + " did not exist or is not a directory."
    next
  end
  Dir.chdir dir
  list_todos(compact)
  Dir.chdir ".."
end

puts $total_todo_count.to_s + " TODOs (" + ($total_todo_count.to_f / $total_line_count.to_f).round(2).to_s + "% of lines) in this project."
