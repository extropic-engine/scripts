#!/bin/sh
app_name="Free Heroes 2.app"
version=`svnversion`

# delete any old files created previously by this script
rm FreeHeroes2*.dmg
rm -rf mac_build
rm -rf "${app_name}"

# build the app bundle
mkdir -p "${app_name}/Contents/MacOS"
mkdir -p "${app_name}/Contents/Resources"
mkdir -p "${app_name}/Contents/Frameworks"
echo -n "APPL????" > "${app_name}/Contents/PkgInfo"
cat > "${app_name}/Contents/Info.plist" <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>CFBundleName</key>
    <string>Free Heroes 2</string>
    <key>CFBundleDisplayName</key>
    <string>Free Heroes 2</string>
    <key>CFBundleDevelopmentRegion</key>
    <string>English</string>
	<key>CFBundleVersion</key>
	<string>${version}</string>
    <key>CFBundleExecutable</key>
    <string>fheroes2</string>
	<key>CFBundleIconFile</key>
	<string>fheroes2.icns</string>
    <key>CFBundleInfoDictionaryVersion</key>
    <string>6.0</string>
    <key>CFBundlePackageType</key>
    <string>APPL</string>
    <key>CFBundleSignature</key>
    <string>????</string>
</dict>
</plist>
EOF
rsync fheroes2 "${app_name}/Contents/MacOS"
rsync -r --exclude=.svn files "${app_name}/Contents/MacOS/"
rsync -r --exclude=.svn image "${app_name}/Contents/MacOS/"
ln -s ../../../fheroes2.key "${app_name}/Contents/MacOS/fheroes2.key"
ln -s ../../../fheroes2.cfg "${app_name}/Contents/MacOS/fheroes2.cfg"
ln -s ../../../maps "${app_name}/Contents/MacOS/maps"
ln -s ../../../data "${app_name}/Contents/MacOS/data"

# download any needed frameworks and copy them into the temp directory
# SDL
wget http://www.libsdl.org/release/SDL-1.2.15.dmg
hdiutil attach -mountpoint sdl SDL-1.2.15.dmg
rsync -rl sdl/SDL.framework "${app_name}/Contents/Frameworks/"
hdiutil detach sdl
rm SDL-1.2.15.dmg

# SDL_mixer
wget http://www.libsdl.org/projects/SDL_mixer/release/SDL_mixer-1.2.12.dmg
hdiutil attach -mountpoint sdl_mixer SDL_mixer-1.2.12.dmg
rsync -rl sdl_mixer/SDL_mixer.framework "${app_name}/Contents/Frameworks/"
hdiutil detach sdl_mixer
rm SDL_mixer-1.2.12.dmg

# SDL_image
wget http://www.libsdl.org/projects/SDL_image/release/SDL_image-1.2.12.dmg
hdiutil attach -mountpoint sdl_image SDL_image-1.2.12.dmg
rsync -rl sdl_image/SDL_image.framework "${app_name}/Contents/Frameworks/"
hdiutil detach sdl_image
rm SDL_image-1.2.12.dmg

# SDL_ttf
wget http://www.libsdl.org/projects/SDL_ttf/release/SDL_ttf-2.0.11.dmg
hdiutil attach -mountpoint sdl_ttf SDL_ttf-2.0.11.dmg
rsync -rl sdl_ttf/SDL_ttf.framework "${app_name}/Contents/Frameworks/"
hdiutil detach sdl_ttf
rm SDL_ttf-2.0.11.dmg

# SDL_net
wget http://www.libsdl.org/projects/SDL_net/release/SDL_net-1.2.8.dmg
hdiutil attach -mountpoint sdl_net SDL_net-1.2.8.dmg
rsync -rl sdl_net/SDL_net.framework "${app_name}/Contents/Frameworks/"
hdiutil detach sdl_net
rm SDL_net-1.2.8.dmg

# libogg?

# create icns file for app
wget http://www.amnoid.de/icns/makeicns-1.1.zip
unzip makeicns-1.1.zip -d makeicns
makeicns/makeicns -in image/fheroes2_32x32.png -out "${app_name}/Contents/Resources/fheroes2.icns"
rm -rf makeicns
rm makeicns-1.1.zip

# copy fheroes2 files into a temporary directory
mkdir mac_build
rsync README mac_build
rsync AUTHORS mac_build
rsync COPYING mac_build
rsync LICENSE mac_build
rsync fheroes2.key mac_build
rsync fheroes2.cfg mac_build
rsync changelog.txt mac_build
rsync -rl "${app_name}" mac_build
mkdir mac_build/maps
mkdir mac_build/data

rm -rf "${app_name}"

# create a DMG file containing all the built files
echo Creating FreeHeroes2-r`svnversion`.dmg...
hdiutil create -srcfolder mac_build -volname "Free Heroes 2" -format UDBZ FreeHeroes2-r`svnversion`.dmg

# clean up temporary files
rm -rf mac_build
