
require 'json'
require 'colorize'

class Taskprint
  def self.main
    json = ARGF.read
    #json = `task export`
    JSON.parse(json).each do |task|
      next if task['status'] == 'completed'
      next if task['status'] == 'deleted'
      puts "- [ ] #{task['description']}"
      #puts "- [ ] #{task['description']}"[0..80] # truncate to 80 lines
      #puts task                                  # print whole data structure
    end
  end
end
