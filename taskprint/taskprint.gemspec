Gem::Specification.new do |s|
  s.name        = 'taskprint'
  s.version     = '0.0.1'
  s.date        = '2016-05-15'
  s.summary     = 'generate printable todo lists from taskwarrior'
  s.description = 'generate printable todo lists from taskwarrior'
  s.authors     = ['extropic-engine']
  s.email       = 'josh@extropicstudios.com'
  s.files       = [
    'lib/taskprint.rb'
  ]
  s.homepage    = 'https://gitlab.com/extropic-engine/scripts'
  s.license     = 'MIT'
  s.executables << 'taskprint'
  s.required_ruby_version = '~> 2.2'
end
