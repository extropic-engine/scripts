#!/usr/bin/env ruby

quit = false
while !quit do
  puts "\nList of available BBSes\n"
  puts '1. bbs.fozztexx.com'.center(80)
  puts '2. QUIT'.center(80)
  choice = gets.to_i
  if choice == 1 then
      system 'telnet bbs.fozztexx.com 9600'
  elsif choice == 2 then
      quit = true
  end
end
